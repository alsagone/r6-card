export enum Side {
  Attacker = 'A',
  Defender = 'D',
  Recruit = 'AD',
}

export class Operator {
  name: string;
  side: Side;
  hasElite: boolean;
  playtime: number;

  constructor(name: string, side: Side, hasElite: boolean, playtime: number) {
    this.name = name;
    this.side = side;
    this.hasElite = hasElite;
    this.playtime = playtime;
  }

  isAttacker(): boolean {
    return this.side !== Side.Defender;
  }

  isDefender(): boolean {
    return this.side !== Side.Attacker;
  }

  comparePlaytime(otherOperator: Operator): number {
    if (this.playtime === otherOperator.playtime) {
      return 0;
    }

    return this.playtime > otherOperator.playtime ? 1 : -1;
  }
}
