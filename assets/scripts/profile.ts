import R6API from 'r6api.js';

import dotenv from 'dotenv';

dotenv.config();

const email = process.env.UBI_EMAIL!;
const password = process.env.UBI_PASSWORD!;

const r6api = new R6API({ email, password });

export enum Platform {
  Playstation = 'psn',
  Xbox = 'xbl',
  PC = 'uplay',
  Discord = 'discord',
  Instagram = 'instagram',
  Twitter = 'twitter',
  Youtube = 'youtube',
}

export class Profile {
  name: string;
  username: string;
  profilePicture: string;
  platform: Platform;
  mainOperator: string;
  attackers: string[];
  defenders: string[];
  ratio: number;
  rank: string;

  constructor(
    name: string,
    username: string,
    profilePicture: string,
    platform: Platform,
    mainOperator: string,
    attackers: string[],
    defenders: string[],
    ratio: number,
    rank: string
  ) {
    this.name = name;
    this.username = username;
    this.profilePicture = profilePicture;
    this.platform = platform;
    this.mainOperator = mainOperator;
    this.attackers = attackers;
    this.defenders = defenders;
    this.ratio = ratio;
    this.rank = rank;
  }
}
export const clearString = (str: string): string => {
  return str
    .normalize('NFD')
    .replace(/[\u0300-\u036f]/g, '')
    .replace('ø', 'o')
    .toLowerCase();
};

export const placeholderProfile = new Profile(
  'Hakim',
  'alsagone',
  'pp.png',
  Platform.PC,
  'Ace',
  ['Ace', 'Gridlock', 'IQ', 'Hibana', 'Capitão'],
  ['Mute', 'Kaid', 'Wamai', 'Aruni', 'Alibi'],
  0.95,
  'Unranked'
);

/*exports.default = async () => {
  const username = 'alsagone';
  const platform = 'uplay';

  const { 0: player } = await r6api.findByUsername(platform, username);
  if (!player) return 'Player not found';

  const { 0: stats } = await r6api.getStats(platform, player.id);
  if (!stats) return 'Stats not found';
  const {
    pvp: { general },
  } = stats;

  return `${player.username} has played ${general.matches} matches.`;
};*/

/*
Find By Username 
[
  {
    id: '0b95544b-0228-49a7-b338-6d15cfbc3d6a',
    userId: '0b95544b-0228-49a7-b338-6d15cfbc3d6a',
    idOnPlatform: '0B95544B-0228-49A7-B338-6D15CFBC3D6A',
    platform: 'uplay',
    username: 'Daniel.Nt',
    avatar: {
      '146': 'https://ubisoft-avatars.akamaized.net/0b95544b-0228-49a7-b338-6d15cfbc3d6a/default_146_146.png',
      '256': 'https://ubisoft-avatars.akamaized.net/0b95544b-0228-49a7-b338-6d15cfbc3d6a/default_256_256.png',
      '500': 'https://ubisoft-avatars.akamaized.net/0b95544b-0228-49a7-b338-6d15cfbc3d6a/default_tall.png'
    }
  }
]
*/
